// The "document" refers to the whole webpage
// The "querySelector" is used to select a specific object (HTML elements) from the webpage (document)
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// The "addEventListener" is a function that takes two arguments
// keyup - string identifying the event
// event => {} - function that the listener will execute once the specified event is triggered
txtFirstName.addEventListener('keyup', (event)=>{
	// the "innerHTML" property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event)=>{
	// The 'event.target' contains the element where the event happened
	console.log(event.target);
	// The "event.target.value" gets the value of the input object
	console.log(event.target.value);
});


const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtLastName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtLastName.value;
});

txtLastName.addEventListener('keyup', (event) =>{
	console.log(event.target);
	console.log(event.target.value);
});